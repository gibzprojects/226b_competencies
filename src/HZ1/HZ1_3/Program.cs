﻿using System;

namespace HZ1_3
{
    class Program
    {
	static void Main(string[] args)
	{
	    Console.WriteLine("Hello World!");

	    Book inEightyDaysAroundTheWorld = new Book()
	    {
		ISBN = Guid.NewGuid().ToString(),
		Title = "In 80 days around the world",
		SubTitle = "By Jules Verne",
		PriceInGBP = 15.99f
	    };

	    JSONBookSaver jsonBookSaver = new JSONBookSaver(inEightyDaysAroundTheWorld);
	    jsonBookSaver.Save();
	}
    }
}
