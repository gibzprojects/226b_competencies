using Newtonsoft.Json;
using System.IO;

namespace HZ1_3
{
    public class JSONBookSaver
    {
	private Book bookToSave;

	public JSONBookSaver(Book bookToSave)
	{
	    this.bookToSave = bookToSave;
	}

	public void Save()
	{
	    string fileName = $"{bookToSave.Title}.json";

	    using (FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate))
	    {
		using (TextWriter textWriter = new StreamWriter(fileStream))
		{
		    JsonSerializer serializer = new JsonSerializer();
		    serializer.Formatting = Formatting.Indented;

		    serializer.Serialize(textWriter, bookToSave);
		}
	    }
	}
    }
}
