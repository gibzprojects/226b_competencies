﻿namespace HZ1_3
{
    public class Book
    {
	public string Title { get; set; }

	public string ISBN { get; set; }

#nullable enable
	public string? SubTitle { get; set; }

	public float PriceInGBP { get; set; }
    }
}
