﻿namespace HZ1_5.TemplateMethod
{
    public class Programmer : Worker
    {
        public override float Work(int workHours = 9)
        {
            base.EnergyLevels -= 1;

            return EnergyLevels;
        }
    }
}
