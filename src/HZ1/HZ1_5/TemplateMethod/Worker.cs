﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ1_5.TemplateMethod
{
    public abstract class Worker
    {
        private float energyLevels = 100f;

        public string FirstName { get; set; }

        public int SurName { get; set; }

        public int Age { get; set; }

        public virtual long WakeUpTimeInUnixMilliSeconds { get; set; } = DateTimeOffset.Parse("9:00").UtcTicks;

        public virtual float EnergyLevels
        {
            get
            {
                return energyLevels;
            }
            set
            {
                energyLevels = value;
            }
        }

        public virtual void WakeUp()
        {
            if (DateTimeOffset.UtcNow.UtcTicks >= WakeUpTimeInUnixMilliSeconds)
            {
                Console.WriteLine("Wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up wake up");
            }
        }

        public virtual float Work(int workHours = 8)
        {
            energyLevels -= workHours + 10;

            return energyLevels;
        }
    }
}
