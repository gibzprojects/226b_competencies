﻿namespace HZ1_5.TemplateMethod
{
    public class ConstructionWorker : Worker
    {
        public override long WakeUpTimeInUnixMilliSeconds { get; set; } = DateTimeOffset.Parse("6:00").UtcTicks;

        public override float Work(int workHours = 8)
        {
            EnergyLevels -= workHours--;

            return EnergyLevels;
        }
    }
}
