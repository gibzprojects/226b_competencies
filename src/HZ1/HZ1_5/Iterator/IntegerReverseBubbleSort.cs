﻿namespace HZ1_5.Iterator
{
    public class IntegerReverseBubbleSort
    {
        public static List<int> SortCollection(List<int> collectionToSort)
        {
            if (collectionToSort.Count != 0 || collectionToSort != null)
            {
                for (int j = collectionToSort.Count - 1; j > 0; j--)
                {
                    for (int i = collectionToSort.Count - 1; i > 0; i--)
                    {
                        int current = collectionToSort[i];
                        int nextPosition = i - 1;
                        if (nextPosition < collectionToSort.Count)
                        {
                            int next = collectionToSort[nextPosition];

                            if (current < next)
                            {
                                int temp = collectionToSort[i];
                                collectionToSort[i] = next;
                            }
                        }
                    }
                }
            }

            return collectionToSort;
        }
    }
}
