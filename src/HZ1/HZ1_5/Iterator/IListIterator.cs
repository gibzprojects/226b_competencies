﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ1_5.Iterator
{
    public interface IListIterator
    {
        IIterableCollection Iterate(IIterableCollection iterableCollection);
    }
}
