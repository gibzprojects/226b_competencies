﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ1_5.Iterator
{
    public class IntegerBubbleSort : IListIterator
    {
        public IIterableCollection Iterate(IIterableCollection iterableCollection)
        {
            return new IntegerCollection();
        }

        public static List<int> SortCollection(List<int> collectionToSort)
        {
            if (collectionToSort.Count != 0 || collectionToSort != null)
            {
                for (int j = 0; j < collectionToSort.Count; j++)
                {
                    for (int i = 0; i < collectionToSort.Count; i++)
                    {
                        int current = collectionToSort[i];
                        int nextPosition = i + 1;
                        if (nextPosition < collectionToSort.Count)
                        {
                            int next = collectionToSort[nextPosition];

                            if (current < next)
                            {
                                int temp = collectionToSort[i];
                                collectionToSort[i] = next;
                            }
                        }
                    }
                }
            }

            return collectionToSort;
        }
    }
}
