﻿namespace HZ1_5.Iterator
{
    public interface IIterableCollection
    {
        IListIterator CreateIterator();
    }
}
