﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ1_5.Iterator
{
    public class IntegerCollection : IIterableCollection
    {
        public IntegerCollection()
        {
            GenerateCollectionData();
        }

        public List<int> CollectionOfIntegers { get; } = new List<int>();
        
        public IListIterator CreateIterator()
        {
            return new IntegerBubbleSort();
        }

        // Generate a 100 sample entries
        private void GenerateCollectionData()
        {
            for (int i = 0; i < 100; i++)
            {
                Random random = new Random();
                CollectionOfIntegers.Add(random.Next(1000, 500000));
            }
        }
    }
}
