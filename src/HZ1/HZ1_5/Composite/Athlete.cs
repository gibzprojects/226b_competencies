﻿using HZ1_5.Composite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ1_5.Composite
{
    // This is our component
    public interface Athlete
    {
        public void DisplayRaceScore();
    }
}
