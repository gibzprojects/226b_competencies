﻿namespace HZ1_5.Composite
{
    public class Teacher : Person, Athlete
    {
        public void DisplayRaceScore()
        {
            Console.WriteLine("My race score is: 12min on 22km");
        }
    }
}
