﻿namespace HZ1_5.Composite
{
    public class Student : Person, Athlete
    {
        public void DisplayRaceScore()
        {
            Console.WriteLine("My race score is: 22min on 22km");
        }
    }
}
