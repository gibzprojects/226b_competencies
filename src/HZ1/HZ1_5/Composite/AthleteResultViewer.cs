﻿namespace HZ1_5.Composite
{
    public class AthleteResultViewer : Athlete
    {
        public List<Athlete> Athletes { get; set; } = new List<Athlete>();

        public void DisplayRaceScore()
        {
            foreach (Athlete athlete in Athletes)
            {
                athlete.DisplayRaceScore();
            }
        }
    }
}
