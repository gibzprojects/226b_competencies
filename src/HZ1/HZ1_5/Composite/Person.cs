﻿namespace HZ1_5.Composite
{
    public class Person
    {
        public string FirstName { get; set; }

        public string SurName { get; set; }

        public int Age { get; set; }
    }
}
