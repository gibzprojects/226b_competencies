﻿namespace HZ1_5.Composite
{
    public class NascarDriver : Person, Athlete
    {
        public void DisplayRaceScore()
        {
            Console.WriteLine("My race score is: 5min on 22km");
        }
    }
}
