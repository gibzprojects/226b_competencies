﻿using HZ1_5.Composite;
using HZ1_5.Observer;

namespace HZ1_5
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            CompositePatternExample();
            IteratorPatternExample();
            ObserverPatternExample();
            TemplateMethodPatternExample();
        }

        private static void CompositePatternExample()
        {
            Console.WriteLine("\n\nComposite design pattern:\n");

            AthleteResultViewer athleteResults = new AthleteResultViewer();
            Student student = new Student();
            Teacher teacher = new Teacher();

            athleteResults.Athletes.Add(student);
            athleteResults.Athletes.Add(teacher);

            AthleteResultViewer extremeAthletes = new AthleteResultViewer();
            NascarDriver nascarDriver = new NascarDriver();

            extremeAthletes.Athletes.Add(nascarDriver);
            athleteResults.Athletes.Add(extremeAthletes);

            athleteResults.DisplayRaceScore();
        }

        private static void IteratorPatternExample()
        {
            Console.WriteLine("\n\nIterator design pattern:");
        }

        private static void ObserverPatternExample()
        {
            Console.WriteLine("\n\nObserver design pattern:\nCausing havoc");

            Wheel wheel = new Wheel();
            EmergencyBreak emergencyBreak = new EmergencyBreak(wheel);
            EmergencySignal emergencySignal = new EmergencySignal(wheel);

            wheel.AddEmergencyObservers(emergencyBreak, emergencySignal);

            Console.WriteLine($"The current wheel speed is: {wheel.SpeedInKmH}");
            wheel.SpeedInKmH = 380;
            Console.WriteLine("\nOh no! I am way to fast! If somebody could just help an programmer without drivers license.");

            Console.WriteLine("\nS E C U R I T Y  C H E C K  I N I T I A T E D --- S E C U R I T Y  C H E C K  I N I T I A T E D --- S E C U R I T Y  C H E C K  I N I T I A T E D --- S E C U R I T Y  C H E C K  I N I T I A T E D\n");
            wheel.SecurityCheck();
        }

        private static void TemplateMethodPatternExample()
        {
            Console.WriteLine("\n\nTemplate method design pattern:");
        }
    }
}