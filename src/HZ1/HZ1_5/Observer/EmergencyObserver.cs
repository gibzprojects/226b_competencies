﻿// See https://aka.ms/new-console-template for more information

namespace HZ1_5.Observer
{
    public abstract class EmergencyObserver
    {
        public abstract void Alert();
    }
}