﻿// See https://aka.ms/new-console-template for more information

namespace HZ1_5.Observer
{
    public class EmergencyBreak : EmergencyObserver
    {
        public EmergencyBreak(Wheel wheel)
        {
            Wheel = wheel;
        }

        public override void Alert()
        {
            NotifiyDriver();
            DriveToEmergencyLane();
            EmergencyStop();
        }

        public Wheel Wheel { get; set; }

        private void NotifiyDriver()
        {
            Console.WriteLine("You are being rescued. Please do not resist.");
        }

        private void DriveToEmergencyLane()
        {
            // TODO: Implement before product release
        }

        private void EmergencyStop()
        {
            Console.WriteLine($"Setting wheel speed to: {Wheel.SpeedInKmH}");
        }
    }
}