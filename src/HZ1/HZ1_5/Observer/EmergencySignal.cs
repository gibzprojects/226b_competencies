﻿// See https://aka.ms/new-console-template for more information
namespace HZ1_5.Observer
{
    public class EmergencySignal : EmergencyObserver
    {
        public EmergencySignal(Wheel wheel)
        {
            Wheel = wheel;
        }

        public Wheel Wheel { get; set; }

        public override void Alert()
        {

        }

        private void ShowEmergencyOnPanel(string emergencyMessage = "EMERGENCY! STOP RIGH NOW")
        {
            Console.WriteLine("\n\nEmergency message:\n" + emergencyMessage + $"\n The speed of the wheels is: {Wheel.SpeedInKmH}");
        }
    }
}