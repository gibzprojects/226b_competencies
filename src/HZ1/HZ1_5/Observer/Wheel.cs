﻿// See https://aka.ms/new-console-template for more information

namespace HZ1_5.Observer
{
    public class Wheel
    {
        public float SpeedInKmH { get; set; } = 30;

        public List<EmergencyObserver> EmergencyObservers { get; set; } = new List<EmergencyObserver>();

        public void AddEmergencyObservers(params EmergencyObserver[] emergencyObservers)
        {
            EmergencyObservers.AddRange(emergencyObservers);
        }

        public bool SecurityCheck()
        {
            if (SpeedInKmH > 250)
            {
                NotifyEmergencyObservers();
                return false;
            }

            return true;
        }

        private void NotifyEmergencyObservers()
        {
            foreach (EmergencyObserver observer in EmergencyObservers)
            {
                observer.Alert();
            }
        }
    }
}