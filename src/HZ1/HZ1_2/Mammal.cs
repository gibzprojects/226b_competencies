﻿namespace HZ1_2
{
    public abstract class Mammal : IMoveable
    {
	public int Age { get; set; }

	public bool IsFurry { get; set; } = false;

	public double Speed { get; set; }

	public void Start()
	{
	    // TODO:
	    // Implement logic for starting movement of a mammal
	}

	public void Stop()
	{
	    Speed = 0;
	}
    }
}
