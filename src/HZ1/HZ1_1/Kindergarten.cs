﻿using System.Collections.Generic;

namespace HZ1_1
{
    public class Kindergarten
    {
	public string Name { get; set; }

	private List<Child> Plagen { get; set; } = new List<Child>();

	public List<Child> AddChildToKindergarten(Child newChild)
	{
	    Plagen.Add(newChild);

	    return Plagen;
	}
    }
}
