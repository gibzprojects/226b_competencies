﻿using System;
using System.Collections.Generic;

namespace HZ1_1
{
    public abstract class Person
    {
	public Person()
	{

	}

	public Person(string firstName, List<string>? otherNames, string surName, int age, DateTime birthDate)
	{
	    FirstName = firstName;
	    OtherNames = otherNames;
	    SurName = surName;
	    Age = age;
	    BirthDate = birthDate;
	}

	public string FirstName { get; set; }

	public List<string> OtherNames { get; set; } = new List<string>();

	public string SurName { get; set; }

	public int Age { get; set; }

	public DateTime BirthDate { get; set; }

	public int AgeByYears(int yearsToAge = 1)
	{
	    Age = Age += yearsToAge;

	    return Age;
	}
    }
}
