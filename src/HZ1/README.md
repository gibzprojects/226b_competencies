# Directory for Competencies HZ1



This directory contains the POC (<strong>P</strong>roof <strong>o</strong>f <strong>C</strong>oncept) to successfully defend the following competencies (DE):

| 1                                                            | 2                                                            | 3                                                            | 4                                                            | 5                                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Ich kann Situationen mit redundanten Informationen in mehreren Klassen durch Einführung einer Basisklasse vermeiden. | Ich kann überprüfen, ob in einer gegebenen Situation Vererbung angebracht ist, oder ob es besser wäre mit Komposition zu arbeiten. | Ich kann ein objektorientiertes Programm mit einigen fachlichen und technischen Klassen entwerfen, welches frei von Redundanzen ist. | Ich kann eine switch-artige Struktur durch Anwendung von polymorphen Variablen (Delegation und Methoden überschreiben) eliminieren und fördere so die Erweiterbarkeit meines Codes. | Ich kann einfachere Design-Patterns, wie Composite, Iterator, Observer, Template Methode etc. anwenden. |
| Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     |

