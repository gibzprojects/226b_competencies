﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ1_4
{
    public abstract class Mammal
    {
        public string Name { get; set; }

        public double Speed { get; set; }

        public int CurrentPosition { get; set; }

        public int TargetPosition { get; set; } = 1;

        public virtual void Move()
        {
            while (CurrentPosition < TargetPosition)
            {
                CurrentPosition++;
            }

            Console.WriteLine("A");
        }
    }
}
