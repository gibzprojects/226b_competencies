﻿using System;

namespace HZ1_4
{
    class Program
    {
        static void Main(string[] args)
        {
            // This program moves mammals

            Mammal sapien = new HomoSapiens()
            {
                Name = "Sapien"
            };

            Mammal flappy = new Bird()
            {
                Name = "Bird"
            };

            MoveMammals(sapien, flappy);
        }

        private static void MoveMammals(params Mammal[] mammalsToMove)
        {
            for (int i = 0; i < mammalsToMove.Length; i++)
            {
                switch (mammalsToMove[i].Name)
                {
                    case "Bird":
                        mammalsToMove[i].Speed++;
                        mammalsToMove[i].Move();
                        break;

                    case "Sapien":
                        mammalsToMove[i].Speed += 2;
                        mammalsToMove[i].Move();
                        break;
                }
            }
        }
    }
}
