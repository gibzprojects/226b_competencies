﻿namespace HZ1_4
{
    public class Bird : Mammal
    {
        public override void Move()
        {
            while (base.CurrentPosition < base.TargetPosition)
            {
                Flap();
                Flap();
            }

            System.Console.WriteLine("C");
        }

        private void Flap()
        {
            Speed += 0.15;
            CurrentPosition += 1;
        }
    }
}