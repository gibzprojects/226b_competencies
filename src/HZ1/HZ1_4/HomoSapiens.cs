﻿namespace HZ1_4
{
    public class HomoSapiens : Mammal
    {
        public override void Move()
        {
            while (base.CurrentPosition < base.TargetPosition)
                Walk();

            System.Console.WriteLine("B");
        }

        private void Walk()
        {
            CurrentPosition++;
            Speed += 2;
        }
    }
}