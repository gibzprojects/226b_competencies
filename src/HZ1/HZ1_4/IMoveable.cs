﻿namespace HZ1_4
{
    public interface IMoveable
    {
        string Name { get; set; }

        double Speed { get; set; }

        int CurrentPosition { get; set; }

        int TargetPosition { get; set; }

        void Move();

        void Stop();
    }
}
