﻿using System.Collections.Generic;

namespace HZ4_1
{
    public class TodoList
    {
	private List<ITask> tasks = new List<ITask>();

	public void AddTask(ITask taskToAdd)
	{
	    tasks.Add(taskToAdd);
	}

	public void RemoveTask(ITask taskToRemove)
	{
	    tasks.Remove(taskToRemove);
	}

	public List<ITask> GetTasks()
	{
	    return tasks;
	}
    }
}
