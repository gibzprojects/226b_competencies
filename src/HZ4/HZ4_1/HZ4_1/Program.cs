using System;

namespace HZ4_1
{
    class Program
    {
	public static void Main(string[] args)
	{
	    HomeWork mathAssignment = new HomeWork();
	    mathAssignment.Deadline = DateTime.Now.AddDays(2);

	    Sports basketBall = new Sports();
	    basketBall.Description = "And i've took that personally - M. Jordan";

	    HomeWork m226B = new HomeWork();
	    m226B.Deadline = DateTime.Now;
	    m226B.Title = "OH MY GOD I COMPLETLY FORGOT THAT I NEED TO FINISH THESE COMPETENCIES";

	    Sports boxing = new Sports();
	    boxing.Title = "Everybody has a plan. Until they get punched in the face. - Mike Tyson";
	    boxing.Title = "There are 10 kind of people in this world. The ones that do understand binary, and the ones that do not.";

	    TodoList todoList = new TodoList();
	    todoList.AddTask(mathAssignment);
	    todoList.AddTask(basketBall);
	    todoList.AddTask(m226B);
	    todoList.AddTask(boxing);
	}
    }
}
