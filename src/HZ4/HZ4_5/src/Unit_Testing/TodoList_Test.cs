using ExampleApp;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Unit_Testing
{
    public class TodoList_Test
    {
	[Fact]
	public void AddTask_ITask_TasksContainsNewITask()
	{
	    TodoList todoList = new TodoList();
	    Mock<ITask> mockedITask = new Mock<ITask>();

	    todoList.AddTask(mockedITask.Object);
	    bool isNewTaskInTasks = todoList.Tasks.Contains(mockedITask.Object);

	    Assert.True(isNewTaskInTasks);
	}

	[Fact]
	public void RemoveTask_ITask_TasksDoNotContainRemovedTask()
	{
	    TodoList todoList = new TodoList();
	    Mock<ITask> mockedITask = new Mock<ITask>();
	    Mock<ITask> mockedITask2 = new Mock<ITask>();
	    Mock<ITask> mockedITaskToRemove = new Mock<ITask>();

	    todoList.AddTask(mockedITask.Object);
	    todoList.AddTask(mockedITask2.Object);
	    todoList.AddTask(mockedITaskToRemove.Object);

	    todoList.RemoveTask(mockedITaskToRemove.Object);
	    bool isTaskInTasks = todoList.Tasks.Contains(mockedITaskToRemove.Object);

	    Assert.False(isTaskInTasks);
	}
    }
}
