﻿using System.Collections.Generic;

namespace ExampleApp
{
    public class TodoList
    {
	private List<ITask> tasks = new List<ITask>();

	public List<ITask> Tasks
	{
	    get
	    {
		return tasks;
	    }
	    set
	    {
		tasks = value;
	    }
	}

	public void AddTask(ITask taskToAdd)
	{
	    tasks.Add(taskToAdd);
	}

	public void RemoveTask(ITask taskToRemove)
	{
	    tasks.Remove(taskToRemove);
	}
    }
}
