﻿namespace ExampleApp
{
    public interface ITask
    {
	string Title { get; set; }

	string Description { get; set; }
    }
}
