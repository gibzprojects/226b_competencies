﻿namespace ExampleApp
{
    public class Homework : ITask
    {
	public string Title { get; set; }

	public string Description { get; set; }
    }
}
