# Directory for Competencies HZ4



This directory contains the POC (**P**roof **o**f **C**oncept) to successfully defend the following competencies (DE):

| 1                                                            | 2                                                            | 3                                                            | 4                                                            | 5                                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Ich kann für eine gegebene Klasse ein Interface definieren und dieses in einer anderen Klasse an Stelle der ersten Klasse einsetzen. Auf diese Weise kann ich die beiden Klassen entkoppeln. | Ich kann für ein gegebenes Interface eine Stub-Klasse implementieren, welche beim Aufruf der vom Interface definierten Methoden fest programmierte Resultate liefert. | Ich kann eine Stub-Klasse in einem Unit-Test einsetzen und so die zu testende Klasse unabhängig von anderen Klassen testen. | Ich kann fortgeschrittene StubKlassen implementieren, welche einen Zustand haben. Dieser wird durch die aufgerufenen Methoden verändert und kann im Unit-Test überprüft werden. | Ich kann in meinen Unit-Tests eine Mocking-Bibliothek, wie moq oder Rhino Mocks, einsetzen und bei damit erzeugten Mock-Objekten die Einhaltung von Testbedingungen überprüfen. |
| Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     |
