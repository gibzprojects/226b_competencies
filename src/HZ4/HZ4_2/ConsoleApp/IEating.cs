﻿// See https://aka.ms/new-console-template for more information
using ConsoleApp;

public interface IEating
{
    void Eat(ref Food foodToEat);

    bool IsEatable(Food foodToCheck);

    void Puke();
}