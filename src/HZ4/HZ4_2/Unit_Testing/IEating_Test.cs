using ConsoleApp;
using Xunit;

namespace Unit_Testing
{
    public class IEating_Test
    {
        public class MouthStub : IEating
        {
            public void Eat(ref Food foodToEat)
            {
                foodToEat = null;
            }

            public bool IsEatable(Food foodToCheck)
            {
                return foodToCheck.Price < 10.30;
            }

            public void Puke()
            {
                // Implement later
            }
        }

        public class AppleStub : Food
        {
            public AppleStub()
            {
                base.Price = 11;
            }
        }

        [Fact]
        public void IsEatable_AlwaysReturnsFalse_FoodIsNotEatable()
        {
            // Arrange
            MouthStub mouth = new MouthStub();
            AppleStub appleToEat = new AppleStub();

            // Act
            bool isEatable = mouth.IsEatable(appleToEat);


            // Assert
            Assert.False(isEatable);
        }
    }
}