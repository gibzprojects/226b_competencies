﻿namespace HZ4_3
{
    public interface IThrowable
    {
        public bool IsThrowable { get; }

        public float AverageThrowDistanceInMeters { get; set; }

        public void Throw();
    }
}