﻿namespace HZ4_3
{
    public class Baby : IThrowable
    {
        public Baby()
        {
            IsThrowable = true;
        }

        public string FirstName { get; set; }

        public string SurName { get; set; }

        public int AgeInMonths { get; set; } = 24;

        public bool IsThrowable { get; }

        public float AverageThrowDistanceInMeters { get; set; } = 100;

        public void Throw()
        {
            // TODO: Implement humane way of throwing child
        }
    }
}