﻿namespace HZ4_3
{
    public class BasketBall : IThrowable
    {
        public string Producer { get; set; }

        public float Air { get; set; }

        public bool IsThrowable { get; }

        public float AverageThrowDistanceInMeters { get; set; } = 20;

        public void Throw()
        {
            // TODO: Ask Jordan to throw a ball
        }
    }
}