using HZ4_3;
using System;
using Xunit;

namespace Unit_Testing
{
    public class Thrower_Test
    {
        private class FootBallStub : IThrowable
        {
            public bool IsThrowable { get; set; } = false;

            public float AverageThrowDistanceInMeters { get; set; } = 100;

            public void Throw()
            {
                System.Console.WriteLine("The football has been thrown");
            }
        }

        [Fact]
        public void Throw_StubWithIThrowableIsThrowableFalse_ReturnsFalse()
        {
            Assert.False(Thrower.Throw(new FootBallStub()));
        }
    }
}