﻿public class Eater
{
    private IEatable îtemsToEat;

    public IEatable ItemToEat
    {
        get
        {
            return îtemsToEat;
        }
        set
        {
            îtemsToEat = value;
        }
    }

    public void Eat()
    {
        îtemsToEat.Eat();
    }
}
