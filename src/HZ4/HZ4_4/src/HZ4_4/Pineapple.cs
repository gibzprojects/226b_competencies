﻿
public class Pineapple : IEatable
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public int HexColor { get; set; } = 0xA4B716;

    public void Eat()
    {
        // Nom nom nom nom
        Console.WriteLine("This is great food!");
    }
}