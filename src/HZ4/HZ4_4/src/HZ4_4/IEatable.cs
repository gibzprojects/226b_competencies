﻿
public interface IEatable
{
    public Guid Id { get; set; }

    public void Eat();
}
