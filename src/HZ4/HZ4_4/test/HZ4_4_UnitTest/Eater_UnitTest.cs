using System;
using Xunit;

namespace HZ4_4_UnitTest
{
    public class Eater_UnitTest
    {
        public class AppleStub : IEatable
        {
            public Guid Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public int MethodCallCount { get; set; }

            public void Eat()
            {
                // Eating
                MethodCallCount++;
            }
        }

        [Fact]
        public void Eat_Eater_StubImplementsIEatableInterfaceMeasuresMethodCallAmount()
        {
            // Arrange
            AppleStub apple = new AppleStub();
            Eater eater = new Eater();
            eater.ItemToEat = apple;

            // Act
            eater.Eat();

            // Assert
            Assert.Equal(1, apple.MethodCallCount);
        }
    }
}