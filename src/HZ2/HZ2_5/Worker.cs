﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HZ2_5
{
    public abstract class Worker
    {
        public string FirstName
        {
            get => default;
            set
            {
            }
        }

        public string SurName
        {
            get => default;
            set
            {
            }
        }

        public int Age
        {
            get => default;
            set
            {
            }
        }

        public Profession Profession
        {
            get => default;
            set
            {
            }
        }

        public virtual System.Collections.Generic.List<HZ2_5.Hobby> Hobbies
        {
            get => default;
            set
            {
            }
        }
    }
}