﻿using HZ2_5;

public class Program
{
    public static void Main(string[] args)
    {
        Programmer dave = new Programmer();
        Programmer jim = new Programmer();

        Story story = new Story();
        story.Name = "As a user, i want to remove friends, so they stop annoying me.";

        Story story2 = new Story();
        story2.Name = "As a user, i want to sign out";

        List<Programmer> programmers = new List<Programmer>() { dave, jim };
        List<Story> userStories = new List<Story>() { story, story2 };

        Sprint sprint = CreateSprint(programmers, userStories);
    }

    private static Sprint CreateSprint(List<Programmer> programmersToAssign, List<Story> storiesForBacklog)
    {
        Sprint sprint = new Sprint();
        sprint.Programmers = programmersToAssign;
        sprint.Stories = storiesForBacklog;

        return sprint;
    }
}