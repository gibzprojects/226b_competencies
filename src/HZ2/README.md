# Directory for Competencies HZ2



This directory contains the POC (<strong>P</strong>roof <strong>o</strong>f <strong>C</strong>oncept) to successfully defend the following competencies (DE):

| 1                                                            | 2                                                            | 3                                                            | 4                                                            | 5                                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Ich kann die statische Struktur eines objektorientierten Programms mit einigen Klassen in einem UMLKlassendiagramm dokumentieren. | Ich kann die Objektsituation eines objektorientierten Programms zu einem bestimmten Zeitpunkt mit einem Speicherdiagramm erklären. | Ich kann in einem UMLSequenzdiagramm den Aufruf von überschriebenen Methoden bei Objekten mit unterschiedlicher Klasse, aber identischer Basisklasse, illustrieren. (Illustration des dynamischen Bindens bei polymorphen Variablen) | Ich kenne die Bedeutung von weiteren UML-Elementen, wie Interfaces, Pakete, Namespace (Klassendiagramm) und kombinierte Fragmente für Schleifen und Entscheidungen (Sequenzdiagramm) und kann damit meine Diagramme noch aussagekräftiger machen. | Ich kann mit einem UML-Tool ein objektorientiertes Modell für ein Programm erstellen und daraus Code erzeugen. Den Code kann ich von Hand zu einem lauffähigen Programm vervollständigen und schliesslich das Modell mit dem fertigen Code synchronisieren. |
| Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     |

