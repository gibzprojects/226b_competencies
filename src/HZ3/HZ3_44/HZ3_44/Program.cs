﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HZ3_44
{
    class Program
    {
	static void Main(string[] args)
	{
	    string selectedOption = Menu();

	    switch(selectedOption)
	    {
		case "1":
		    EncryptionMenu();
		    break;
		case "2":
		    DecryptionMenu();
		    break;
	    }
	}

	private static string Menu()
	{
	    Console.WriteLine("Do you want to encrypt or decrypt a message?");
	    Console.WriteLine("\n[1] Encrypt\n[2] Decrypt\n\n");

	    return GetUserInput();
	}

	private static void EncryptionMenu()
	{
	    Console.WriteLine("Hello there! Please enter the secret message you'd like to store. After you finished typing, hit enter.");
	    string message = GetUserInput();

	    Console.WriteLine("\nNeat! Now enter a password to secure this message with:");
	    string password = GetUserInput();

	    byte[] messageBytes = GetBytesFromString(message);
	    SecretMessage secretMessage = EncryptMessage(password, messageBytes);
	    string encodedEncryptedMessage = EncodeBytesToString(secretMessage.Message);

	    Console.WriteLine($"This is the Id of your encrypted message: {secretMessage.Id}\nYou must keep it, otherwise you won't be able to search for it anymore.");

	    SaveSecretMessage(secretMessage);
	}

	private static void DecryptionMenu()
	{
	    Console.WriteLine("Hello there! Please enter the id of the message you'd like to decrypt.");
	    string messageId = GetUserInput();

	    Console.WriteLine("\nNow enter the password you used to encrypt the message with:");
	    string password = GetUserInput();

	    SecretMessageStore secretMessageStore = new SecretMessageStore();
	    SecretMessage secretMessage = secretMessageStore.GetSecretMessageById(messageId);

	    if (secretMessage != null)
	    {
		AesDecryptor aesDecryptor = new AesDecryptor(secretMessage);
		aesDecryptor.Decrypt(password);

		Console.WriteLine($"Here is your decrypted message: {EncodeBytesToString(aesDecryptor.DecryptedMessage)}");
	    }
	    else
	    {
		Console.WriteLine("\n\nA message with this id couldn't be found. Please try again.");
		Process.GetCurrentProcess().Refresh();
	    }
	}

	private static string GetUserInput()
	{
	    Console.Write("> ");
	    return Console.ReadLine();
	}

	private static byte[] GetBytesFromString(string message)
	{
	    return Encoding.UTF8.GetBytes(message);
	}

	private static SecretMessage EncryptMessage(string password, byte[] messageToEncrypt)
	{
	    AesEncryptor aesEncryptor = new AesEncryptor(messageToEncrypt);
	    aesEncryptor.GenerateSalt();
	    byte[] key = aesEncryptor.SaltPassword(password);
	    aesEncryptor.Encrypt(key);

	    return CreateSecretMessage(aesEncryptor);
	}

	private static SecretMessage CreateSecretMessage(AesEncryptor aesEncryptor)
	{
	    SecretMessage secretMessage = new SecretMessage()
	    {
		Id = Guid.NewGuid(),
		IV = aesEncryptor.IV,
		KeySalt = aesEncryptor.Salt,
		Message = aesEncryptor.EncryptedMessage
	    };

	    return secretMessage;
	}

	private static string EncodeBytesToString(byte[] bytesToEncode)
	{
	    return Encoding.UTF8.GetString(bytesToEncode);
	}

	private static void SaveSecretMessage(SecretMessage secretMessageToSave)
	{
	    SecretMessageStore saver = new SecretMessageStore();
	    saver.Save(secretMessageToSave);
	}
    }
}
