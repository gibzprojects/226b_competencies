﻿using System.IO;
using System.Security.Cryptography;

namespace HZ3_44
{
    public class AesEncryptor : IAesEncryptor
    {
	public AesEncryptor(byte[] messageToEncrypt)
	{
	    MessageToEncrypt = messageToEncrypt;
	}

	public byte[] MessageToEncrypt { get; set; }

	public byte[] Salt { get; set; }

	public byte[] IV { get; set; }

	public byte[] EncryptedMessage { get; set; }

	public void GenerateSalt()
	{
	    byte[] saltBytes = new byte[32];

	    using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
	    {
		rng.GetNonZeroBytes(saltBytes);
	    }

	    Salt = saltBytes;
	}
	public byte[] SaltPassword(string password)
	{
	    Rfc2898DeriveBytes derivedBytes = new Rfc2898DeriveBytes(password, Salt);
	    byte[] saltedPassword = derivedBytes.GetBytes(32);

	    return saltedPassword;
	}

	/// <summary>
	/// Encrypts the MessageToEncrypt byte array and assigns the result to the EncryptedMessage array
	/// </summary>
	/// <param name="key">The key to use for encryption</param>
	public void Encrypt(byte[] key)
	{
	    Aes aes = AesManaged.Create();
	    aes.Key = key;
	    aes.GenerateIV();
	    IV = aes.IV;

	    using (ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV))
	    {
		using (MemoryStream ms = new MemoryStream())
		{
		    using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
		    {
			cs.Write(MessageToEncrypt);
		    }

		    EncryptedMessage = ms.ToArray();
		}
	    }
	}
    }
}
