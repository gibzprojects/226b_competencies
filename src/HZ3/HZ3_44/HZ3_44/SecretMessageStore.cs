﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HZ3_44
{
    public class SecretMessageStore
    {
	public SecretMessageStore()
	{

	}

	public void Save(SecretMessage secretMessageToSave)
	{
	    List<SecretMessage> allSecretMessages = GetAllSecretMessages();

	    if (allSecretMessages == null)
	    {
		allSecretMessages = new List<SecretMessage>();
	    }

	    using (FileStream fs = new FileStream("secretMessageStore.json", FileMode.OpenOrCreate))
	    {
		using (TextWriter tw = new StreamWriter(fs))
		{
		    JsonSerializer serializer = new JsonSerializer();

		    serializer.Formatting = Formatting.Indented;

		    allSecretMessages.Add(secretMessageToSave);
		    serializer.Serialize(tw, allSecretMessages);
		}
	    }
	}

	public SecretMessage? GetSecretMessageById(string messageId)
	{
	    List<SecretMessage> secretMessages = GetAllSecretMessages();
	    SecretMessage secretMessage = null;

	    if (secretMessages != null && secretMessages.Count > 0)
	    {
		secretMessage = secretMessages
		    .Where(sm => sm.Id.ToString() == messageId)
		    .FirstOrDefault();

		return secretMessage;
	    }

	    return secretMessage;
	}

	private List<SecretMessage> GetAllSecretMessages()
	{
	    List<SecretMessage> secretMessages = new List<SecretMessage>();

	    using (FileStream fs = new FileStream("secretMessageStore.json", FileMode.OpenOrCreate))
	    {
		using (TextReader tr = new StreamReader(fs))
		{
		    JsonSerializer serializer = new JsonSerializer();

		    secretMessages = (List<SecretMessage>)serializer.Deserialize(tr, typeof(List<SecretMessage>));
		}
	    }

	    return secretMessages;
	}
    }
}
