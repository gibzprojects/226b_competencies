﻿namespace HZ3_44
{
    public interface IAesEncryptor
    {
	byte[] MessageToEncrypt { get; set; }

	byte[] Salt { get; set; }

	byte[] EncryptedMessage { get; set; }

	void GenerateSalt();

	byte[] SaltPassword(string password);

	void Encrypt(byte[] key);
    }
}
