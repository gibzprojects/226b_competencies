﻿using System.IO;
using System.Security.Cryptography;

namespace HZ3_44
{
    public class AesDecryptor : IAesDecryptor
    {
	public AesDecryptor(SecretMessage secretMessage)
	{
	    SecretMessage = secretMessage;
	}

	public byte[] DecryptedMessage { get; set; }

	public SecretMessage SecretMessage { get; set; }

	public void Decrypt(string password)
	{
	    byte[] saltedPassword = SaltPassword(password);

	    Aes aes = AesManaged.Create();
	    aes.Key = saltedPassword;
	    aes.IV = SecretMessage.IV;

	    using (ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV))
	    {
		using (MemoryStream ms = new MemoryStream())
		{
		    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
		    {
			cs.Write(SecretMessage.Message);
		    }

		    DecryptedMessage = ms.ToArray();
		}
	    }
	}

	private byte[] SaltPassword(string passwordToSalt)
	{
	    Rfc2898DeriveBytes derivedBytes = new Rfc2898DeriveBytes(passwordToSalt, SecretMessage.KeySalt);
	    return derivedBytes.GetBytes(32);
	}
    }
}
