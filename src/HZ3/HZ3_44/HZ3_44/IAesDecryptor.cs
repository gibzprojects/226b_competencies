﻿namespace HZ3_44
{
    public interface IAesDecryptor
    {
	public byte[] DecryptedMessage { get; set; }

	SecretMessage SecretMessage { get; set; }

	void Decrypt(string password);
    }
}
