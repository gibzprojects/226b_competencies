﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HZ3_44
{
    public class SecretMessage
    {
	public Guid Id { get; set; }

	public byte[] Message { get; set; }

	public byte[] IV { get; set; }

	public byte[] KeySalt { get; set; }
    }
}
