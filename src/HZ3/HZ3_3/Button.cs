﻿using System;

namespace HZ3_3
{
    // Implementation example: https://www.youtube.com/watch?v=gYC-9PUGwDI
    public class Button
    {
	public event EventHandler ClickEvent;

	public void OnClick()
	{
	    // WTF is ".Invoke()"?
	    ClickEvent?.Invoke(this, EventArgs.Empty);
	}
    }
}
