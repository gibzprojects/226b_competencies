﻿namespace HZ3_3
{
    public class StateChanger
    {
	public delegate void UpdateStateDel(State stateToChangeTo);
	public event UpdateStateDel UpdateStateEvent; // Publisher

	public StateChanger()
	{
	    UpdateStateEvent += UpdateState;
	}

	public State State { get; set; }

	// Subscriber
	public void OnStateUpdated(State stateToChangeTo)
	{
	    UpdateStateEvent?.Invoke(stateToChangeTo);
	}

	private void UpdateState(State stateToChangeTo)
	{
	    State = stateToChangeTo;
	}
    }
}
