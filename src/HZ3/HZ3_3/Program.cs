﻿using System;

namespace HZ3_3
{
    class Program
    {
	static void Main(string[] args)
	{
	    Console.WriteLine("Hello world!");
	    ButtonClick();
	    StateChanger();
	}

	private static void ButtonClick()
	{
	    Button button = new Button();

	    // Anonymous method
	    button.ClickEvent += (s, args) =>
	    {
		Console.WriteLine("Button was clicked!");
	    };

	    button.OnClick();
	}

	private static void StateChanger()
	{
	    StateChanger stateChanger = new StateChanger()
	    {
		State = State.Normal
	    };

	    Console.WriteLine($"This is the current state: {stateChanger.State}");

	    stateChanger.OnStateUpdated(State.Excited);

	    Console.WriteLine($"This is the state after calling the event: {stateChanger.State}");
	}
    }
}
