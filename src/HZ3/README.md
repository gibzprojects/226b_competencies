# Directory for Competencies HZ3



This directory contains the POC (<strong>P</strong>roof <strong>o</strong>f <strong>C</strong>oncept) to successfully defend the following competencies (DE):

| 1                                                            | 2                                                            | 3                                                            | 4                                                            | 5                                                            |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Ich kann mit C# eine Basisklasse und davon abgeleitete Klassen mit Instanzvariablen, und Konstruktoren implementieren. Ich vermeide dabei Redundanz im Code. | Ich kann Methoden in Basisklassen deklarieren / implementieren und in abgeleiteten Klassen implementieren / überschreiben. Ich verwende dann die Basisklasse zur Deklaration von polymorphen Variablen. | Ich kann ein Listener-Interface implementieren oder eine Adapterklasse erweitern und damit Ereignisse in C# Programmen behandeln. | Ich kann ein lauffähiges objektorientiertes C# Programm mit mehreren Klassen und einer minimalen grafischen Benutzeroberfläche oder mit Konsolenbedienung implementieren. | Ich mache ausgiebigen Gebrauch von weiteren C#-Sprachmitteln und den in der .Net-Bibliothek verfügbaren Klassen. |
| Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     | Defended                                                     |

