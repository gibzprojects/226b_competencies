﻿using System.Collections.Generic;

namespace HZ3_1
{
    public class User
    {
	public string _firstName;

	public List<string> _middleNames = new List<string>();

	public string _surName;

	public string _userName;

	public int _age;

	public User()
	{

	}

	public User(string firstName, List<string> middleNames, string surName, string userName, Role role)
	{
	    _firstName = firstName;
	    _middleNames = middleNames;
	    _surName = surName;
	    _userName = userName;
	}

#nullable enable
	public virtual List<Role> Roles { get; set; } = new List<Role>();
    }
}
