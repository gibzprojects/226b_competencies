﻿using System;
using System.Collections.Generic;

namespace HZ3_1
{
    public class Admin : User
    {
	public Admin()
	{

	}

	public Admin(string firstName, List<string> middleNames, string surName, string userName, Role role) : base(firstName, middleNames, surName, userName, role)
	{

	}

	public override List<Role> Roles { get; set; } = new List<Role>()
	{
	    new Role()
	    {
		Id = Guid.NewGuid(),
		Name = "Admin"
	    }
	};
    }
}
