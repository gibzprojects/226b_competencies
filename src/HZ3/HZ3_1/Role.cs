﻿using System;

namespace HZ3_1
{
    public class Role
    {
	public Guid Id { get; set; }

	public string Name { get; set; }
    }
}
