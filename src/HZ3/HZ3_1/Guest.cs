﻿using System;

namespace HZ3_1
{
    public class Guest : User
    {
	public Guest()
	{
	    Role guestRole = CreateGuestRole("Guest");
	    AddGuestRoleToRoles(guestRole);
	}

	private Role CreateGuestRole(string roleName)
	{
	    return new Role()
	    {
		Id = Guid.NewGuid(),
		Name = roleName
	    };
	}

	private void AddGuestRoleToRoles(Role guestRole)
	{
	    Roles.Add(guestRole);
	}
    }
}
