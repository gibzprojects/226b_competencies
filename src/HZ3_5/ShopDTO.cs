﻿namespace HZ3_5
{
    public class ShopDTO
    {
	public string ShopName { get; set; }

	public int ShopEmployeCount { get; set; }

	public string LocationCountry { get; set; }

	public string LocationCity { get; set; }

	public int LocationPostalCode { get; set; }

	public string LocationStreetName { get; set; }

	public int LocationHouseNumber { get; set; }
    }
}
