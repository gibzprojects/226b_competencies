﻿namespace HZ3_5
{
    public class Shop
    {
	public string Name { get; set; }

	public int EmployeCount { get; set; }

	public Location Location { get; set; }
    }
}
