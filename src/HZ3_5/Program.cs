﻿using AutoMapper;
using System;

namespace HZ3_5
{
    class Program
    {
	static void Main(string[] args)
	{
	    var configuration = new MapperConfiguration(config =>
	    {
		config.CreateMap<Shop, ShopDTO>().ReverseMap();
	    });

	    configuration.AssertConfigurationIsValid();
	    IMapper mapper = configuration.CreateMapper();

	    Shop shop = new Shop()
	    {
		Name = "MascaraShop"
	    };

	    Location location = new Location()
	    {
		Country = "France",
		City = "Paris",
		PostalCode = 3404,
		StreetName = "SomeFrenchName",
		HouseNumber = 77
	    };

	    shop.Location = location;

	    ShopDTO shopDto = MapShopToShopDTO(mapper, shop);
	    Console.WriteLine(
		$"Name: {shopDto.ShopName}" +
		$"\nEmployee count: {shopDto.ShopEmployeCount}" +
		$"\nCountry: {shopDto.LocationCountry}" +
		$"\nCity: {shopDto.LocationCity}" +
		$"\nPostal code: {shopDto.LocationPostalCode}" +
		$"\nStreet name: {shopDto.LocationStreetName}" +
		$"\nHouse number: {shopDto.LocationHouseNumber}");

	    Shop mappedShop = MapShopDTOToShop(mapper, shopDto);
	    Console.WriteLine(
		$"\n\nCountry: {shop.Location.Country}" +
		$"\nCity: {shop.Location.City}" +
		$"\nPostal code: {shop.Location.PostalCode}" +
		$"\nStreet name: {shop.Location.StreetName}" +
		$"\nHouse number: {shop.Location.HouseNumber}\n");
	}

	private static ShopDTO MapShopToShopDTO(IMapper mapper, Shop shopToMap)
	{
	    return mapper.Map<ShopDTO>(shopToMap);
	}

	private static Shop MapShopDTOToShop(IMapper mapper, ShopDTO createShopToMap)
	{
	    return mapper.Map<Shop>(createShopToMap);
	}
    }
}
