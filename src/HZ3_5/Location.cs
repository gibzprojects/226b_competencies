﻿namespace HZ3_5
{
    public class Location
    {
	public string Country { get; set; }

	public string City { get; set; }

	public int PostalCode { get; set; }

	public string StreetName { get; set; }

	public int HouseNumber { get; set; }
    }
}
