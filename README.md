# Hello 226B

This repository is used to host all the projects required to achieve competencies (Kompetenzen) in the Module 226B. Each commit is signed as a identity proof.



Table of contents:

[TOC]

## Git Workflow

I am working with the GitFlow workflow. A detailed explanation can be found here: <strong><a href="https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow" target="_blank">Atlassian Gitflow</a></strong>.

### Why?

+ It sounded cool.
+ It is easy to use.
+ I like branching. We do a lot of branching with Gitflow.



## Statuses

The following statuses are used to mark a competency:

| Name     | Meaning                                                      |
| -------- | ------------------------------------------------------------ |
| ToDo     | The work on this competency has not been started yet.        |
| Doing    | This competency is currently being worked on.                |
| Done     | This competency has been finished.                           |
| Fixing   | This competency is being fixed (Only used for big problems). |
| Defended | This competency was tested by the teacher.                   |

